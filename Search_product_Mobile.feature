Feature: Search Product

@Positive_case
   Scenario Outline: TC-Sch-001 User want searches for available items in the system
     Given user successfully log in to the apps
     And user is on the dashboard page
     When user inputs the keyword "<value>" in the search field
     And user click "Enter" on the keyboard
     Then the system will display a list of products with name "<output>"
     Examples:
              | case_id    | value      |  output  | 
              | TC-ScH-001 |    Kucing  |    Kucing|