@logout
Feature: Log Out

    @positive_test
    Scenario: TC.Log.002.001-User want to log out
        Given user successfully Login
        When user click Akun
        And user click Keluar
        Then user must get a pop-up message successfully log out
        And user successfully log out