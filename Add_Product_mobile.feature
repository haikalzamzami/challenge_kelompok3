Feature: Add Product

    @positive_case        
        Scenario: TSM.Prod.001.001 The user as a seller wants to Add new Product data
        Given The user is already login the app
        And Tap on "+" button in the bottom of dashboard page
        When user redirected to Add Product page
        And user input product's name in Nama Produk field 
        And user input the price in Harga Produk field
        And user choose the category of the product in Kategori field
        And user input the location of the product in Lokasi field
        And user input the description of the product in Deskripsi field
        And user is uploading the product picture
        And user tap Terbitkan button
        Then Product data saved 
        And The product will be showing in Dashboard Page
        And The New Product data will be added to Daftar Jual Saya page

    @negative_case
        Scenario: TSM.Prod.001.002 and TSM.Prod.001.003 The user as a seller wants to Add new Product data but leaves the some field empty
        Given The user is already login the website
        And Tap on "+" button
        When user redirected to Add Product page
        And user did not input the "<data>" in "<field>" field 
        And user choose the category of the product in Kategori field
        And user input the location of the product in Lokasi field
        And user input the description of the product in Deskripsi field
        And user is uploading the product picture
        And user tap Terbitkan button
        Then Product data will not be saved 
        And Under the "<field>" field will show a message "<message>"
        And The user will stay on Add product page  

    Examples:
    | case_id            | data            | field         | message                         |
    | TSM.Prod.001.002   | product's name  | Nama Produk   | Nama Produk tidak boleh kosong  |
    | TSM.Prod.001.003   | product's price | Harga Produk  | Harga Produk tidak boleh kosong |