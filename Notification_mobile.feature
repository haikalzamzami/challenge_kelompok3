Feature:  Notification 

@Positive_case
   Scenario Outline:TC-Notif-001 User wants to access the notification menu
     Given user successfully log in to the apps
     And user is on the dashboard page
     When user click menu "notifikasi" on bottom bar dashboard
     Then The system will display a list of notifications received by the user