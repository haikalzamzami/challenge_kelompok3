Feature:  Detail Product
 @Positive_case
 Scenario Outline: TC-detail.product-001 User wants to see product details
     Given user successfully log in to the apps
     And user is on the dashboard page
     When user click product "<name.product>" 
     Then the system will display a detail page of product "<name.product>"
     And the system will display a image of products 
     And the system will display a price of product 
     And the system will display a seller name of product 
     And the system will display a category of product
     And the system will display a description  of product 
     And the system will display a button "Saya Tertarik dan Ingin Nego"
     Examples:
              | case_id               | name.product | 
              | TC-detail.product-001 |    Perfume  | 