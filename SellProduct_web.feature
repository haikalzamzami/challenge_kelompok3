Feature: Sell Product

    @positive_case
    Scenario: TC.Sell.001.001-User want to Sell Product after Login
        Given user fill the login page with valid credentials
        And user successfully login
        When user view the notification
        And user accept the offer from buyer  
        Then user can see pop up message that product has successfully sold


    @negative_case
    Scenario: TC.Sell.001.002-User want to Sell Product before Login
        Given user want to sell product without login
        When user go to homepage for create the product
        And user click the Sell button
        Then user can see pop up alert message that user must login first     
