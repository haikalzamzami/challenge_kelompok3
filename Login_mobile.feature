Feature: Login
    Background: The user access login page
        Given The user is already on the login page
        When Run the Second Hand application
        And Click the Account menu
        And Click the Sign in button

    @positive_case
    Scenario: TC.Log.002.001-The user has successfully logged in with valid information
        When Click the Sign in button
        Then The user has successfully logged in to his account and is redirected to the My Account page

    @negative_case
    Scenario: TC.Log.002.002-The user failed to Login with invalid information
        When Fill in the login form with invalid information, such as an unregistered email address and password.
        And Click the Sign in button
        Then An error message appears indicating that Incorrect email or password
        And The user cannot login to his account and remains on the login page.

    @negative_case
    Scenario: TC.Log.002.003-The user failed to login because the email address is empty
        When Fill in the login form with a blank email
        And Click the Sign in button
        Then An error message appears indicating that Email cannot be empty
        And The user cannot login to his account and remains on the login page

    @negative_case
    Scenario: TC.Log.002.004-The user failed to login because the password is empty
        When Fill in the login form with an empty password
        And Click the Sign in button
        Then An error message appears indicating that Password cannot be empty
        And The user cannot login to his account and remains on the login page
        
