Feature: Buy Product

    @positive_case
    Scenario: TC.Buy.001.001-User want to Buy Product after Login
        Given user fill the login page with valid credentials
        And user successfully login
        When user choose the Product that want to Buy
        And user input the nominal of product offer  
        Then user successfully offer the product


    @negative_case
    Scenario: TC.Buy.001.002-User want to Buy Product before Login
        Given user buy product without login
        And user choose the Product that want to Buy
        When user input the nominal of product offer  
        Then user can see alert message that user must login first     

    @negative_case
    Scenario: TC.Buy.001.003-User want to Buy Product with empty field on the offer page
        Given user fill the login page with valid credentials
        And user successfully login
        When user choose the Product that want to Buy
        And user input the nominal of product offer with 0  
        Then user can see alert message that user must input the nominal offer 