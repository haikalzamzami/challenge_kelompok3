Feature: Delete Product
 @Positive_case
   Scenario Outline:TC-Delete.Product-001 User wants to delete product
     Given user successfully add product "<productname>"
     And user is on the dashboard page
     When User accsess menu Daftar jual Saya
     And User click detail product "<productname>"
     And User click delete
     Then The system will delete the product "<productname>"
     And The product "<productname>" will not appear on the dashboard and menu Daftar Jual saya
     Examples:
         |       case_id         | productname| 
         | TC-Delete.Product-001 |    kacang  | 