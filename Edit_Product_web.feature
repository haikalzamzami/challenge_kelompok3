Feature: Edit Product
    Background: The user want to access Edit product page
        Given The user is already login the website
        And The user is already Add the product data
        When User click on Hamburger Button in top right dashboard page
        And User chooses the desired product data to edit
        And User click on Edit Button

    @positive_case        
        Scenario: TSW.Edit.001.001 The user as a seller wants to Edit Product data
        When the user in Edit Product page
        And User change the Name of the product in "Nama Produk" field
        And User change Price of the product in "Harga Produk" field above Rp 0;
        And User change the category of the product in "Kategori" field
        And User change Description of the product in "Deskripsi" field
        And User change a product picture in .jpeg file format size under 5MB
        And User Click on "Simpan" button
        Then The Product data will be change and saved
        And The Product with new data will be updated on Dashboard Page

    @positive_case
        Scenario: TSW.Edit.001.002 The user as a seller wants to add another product picture when editing product data 
        When the user in Edit Product page
        And User change the Name of the product in "Nama Produk" field
        And User change Price of the product in "Harga Produk" field above Rp 0;
        And User change the category of the product in "Kategori" field
        And User change Description of the product in "Deskripsi" field
        And User add another product picture in .jpeg file format size under 5MB
        And User Click on "Simpan" button
        Then The Product data will be change and saved
        And The Product with new data will be updated on Dashboard Page