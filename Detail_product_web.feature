Feature:  Detail Product
 @Positive_case
   Scenario Outline: TC-detail.product-001 User wants to see product details
     Given user is on the dashboard page
     When user click product "<nameproduct>" 
     Then the system will display a detail of products "<nameproduct>"
     And the system will display a image of products 
     And the system will display a description  of product 
     And the system will display a price of product 
     And the system will display a name seller of product 
     And the system will display a seller location of product 
     And the system will display a button saya tertarik dan ingin nego
     Examples:
              | case_id               |     nameproduct     |  
              | TC-detail.product-001 |    Baju Eiger MN02  |