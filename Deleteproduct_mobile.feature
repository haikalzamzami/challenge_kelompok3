Feature: Delete Product
@Positive_case
   Scenario Outline:TC-Delete.Product-001 User wants to delete product
     Given user successfully log in to the apps
     And user successfully add product "<productname>"
     And product "<productname>" status is not yet in demand or bid by the buyer
     And user is on the dashboard page
     When User click menu "akun"
     And user click menu "Daftar jual saya"
     And user click tab "produk"
     And user select product "<productname>"
     And user click  symbol "Recycle Bin"
     And user select and click "hapus"
     Then the system will delete the product "<productname>"
     And the system will display allert "produk berhasil dihapus"
     And the product "<productname>" will not appear on the dashboard and menu "Daftar Jual saya"
     Examples:
         |       case_id         | productname| 
         | TC-Delete.Product-001 |    musang  | 
  

@Negative_case
   Scenario Outline:TC-Delete.Product-002 User wants to cancel the delete product action
     Given user successfully log in to the apps
     And user successfully add product "<productname>"
     And product "<productname>" status is not yet in demand or bid by the buyer
     And user is on the dashboard page
     When user click menu "akun"
     And user click menu "Daftar jual saya"
     And user click tab "produk"
     And user select product "<productname>"
     And user click  symbol "Recycle Bin"
     And user select and click "batalkan"
     Then user successfully does not continue the delete product process
     And product "<productname>" is not deleted by the system
     And the system will redirect to the "produk list" page, in the "Daftar Jual Saya menu"
     Examples:
         |       case_id         | productname| 
         | TC-Delete.Product-002 |   Kipas    | 
  