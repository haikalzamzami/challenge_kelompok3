Feature: Add Product

    @positive_case        
        Scenario: TSW.Prod.001.001 - The user as a seller wants to Add new Product data
        Given The user is already login the website
        And Click on "+ Jual" button
        When user redirected to Add Product page
        And user input product's name in Nama Produk field 
        And user input the price in Harga Produk field
        And user choose the category of the product in Kategori field
        And user input the description of the product in Deskripsi field
        And user is uploading the product picture
        And user click Simpan button
        Then Product data saved 
        And The product will be showing in Dashboard Page
        And The New Product data will be added to Daftar Jual Saya page
        And A notification of the product will appear at the top right corner in Dashboard Page

    @negative_case
        Scenario: TSW.Prod.001.002 and TSW.Prod.001.003 - The user as a seller wants to Add new Product data but leaves the some field empty
        Given The user is already login the website
        And Click on "+ Jual" button
        When user redirected to Add Product page
        And user did not input the "<data>" in "<field>" field 
        And user choose the category of the product in Kategori field
        And user input the description of the product in Deskripsi field
        And user is uploading the product picture
        And user click Simpan button
        Then Product data will not be saved 
        And Under the "<field>" field will show a message "<message>"
        And The user will stay on Add product page  

    Examples:
    | case_id            | data            | field         | message              |
    | TSW.Prod.001.002   | product's name  | Nama Produk   | Name can't be blank  |
    | TSW.Prod.001.003   | product's price | Harga Produk  | Price can't be blank |