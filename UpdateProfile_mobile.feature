@update_profile
Feature: Update Profile
    Background: User successfully Login, click Akun and click Edit icon
        Given user successfully Login
        When user click Akun
        And user click Edit icon

    @positive_test
    Scenario: TC.Upd.Prof.001.001-User update the profile
        When user click Nama field
        And user update Nama field
        And user click Simpan button
        Then user get see a pop-up message informing that Profil berhasil diperbaharui
        And user successfully update the profile
    
    @negative_test
    Scenario: TC.Upd.Prof.001.002-User update the profile without filling in the Name field
        When user click Nama field
        And user leave Nama field empty
        And user click Simpan button
        Then user get see a pop-up message informing that Wajib diisi
        And user failed to update the profile
    
    @negative_test
    Scenario: TC.Upd.Prof.001.003-User update the profile without filling in the Phone Number field
        When user click Nomor Hp field
        And user leave Nomor Hp field empty
        And user click Simpan button
        Then user get see a pop-up message informing that Wajib diisi
        And user failed to update the profile

    @negative_test
    Scenario: TC.Upd.Prof.001.004-User update the profile with input spaces in the Address field
        When user click Alamat field
        And user input spaces in Alamat field
        And user click Simpan button
        Then user get see a pop-up message informing that Wajib diisi
        And user failed to update the profile