@update_profile
Feature: Update Profile
    Background: User is already on Homepage and click Akun on Header
        Given user is already on Homepage
        When user click Akun on Header

    @positive_test
    Scenario: TC.Upd.Prof.001.001-User update the profile
        When user update Nama field
        And user click Simpan button
        Then user successfully update the profile

    @negative_test
    Scenario: TC.Upd.Prof.001.002-User update the profile without filling in the Name field
        When user leave Nama field empty
        And user click Simpan button
        Then user get see a pop-up message informing that Please fill out this field
        And user failed to update the profile
    
    @negative_test
    Scenario: TC.Upd.Prof.001.003-User update the profile without selecting the City
        When user update without select Kota
        And user click Simpan button
        Then user get see a pop-up message informing that Please select an item in the list
        And user failed to update the profile

    @negative_test
    Scenario: TC.Upd.Prof.001.004-User update the profile with too large an image
        When user upload too large an image
        And user click Simpan button
        Then user must get a pop-up message failed to upload too large an image profile
        And user failed to update the profile