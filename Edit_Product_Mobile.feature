Feature: Edit Product
    Background: The user want to access Edit product page
        Given The user is already login the app
        And The user is already Add the product data
        When User click on " Akun " Button in bottom right of the app
        And User click on "Daftar Jual Saya" menu
        And User tap to the desired product data to edit

    @positive_case        
        Scenario: TSM.Edit.001.001 The user as a seller wants to Edit Product data
        When the user in Edit Product page
        And User change the Name of the product in "Nama Produk" field
        And User change Price of the product in "Harga Produk" field above Rp 0;
        And User change the category of the product in "Kategori" field
        And User change the location of the product in "Lokasi" field
        And User change Description of the product in "Deskripsi" field
        And User change a product picture in .jpeg file format size under 5MB
        And User tap "Perbarui Produk" button
        Then The Product data will be change and saved
        And The Product with new data will be updated on Dashboard Page        

    @negative_case
        Scenario: TSM.Edit.001.002 The user as a seller wants to change product picture with the size above 5 MB
        When the user in Edit Product page
        And User change the Name of the product in "Nama Produk" field
        And User change Price of the product in "Harga Produk" field above Rp 0;
        And User change the category of the product in "Kategori" field
        And User change the location of the product in "Lokasi" field
        And User change Description of the product in "Deskripsi" field
        And User change a product picture in .jpeg file format size above 5MB
        And User tap "Perbarui Produk" button
        Then The Product data will not be saved
        And Show an error message "Request Entitiy Too Large"
        And User will stay in Product page